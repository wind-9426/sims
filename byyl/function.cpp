﻿/*  
 @author : Song Jiaqi 
 @date : 2022-05-14 22:33  
*/

#include "function.h"

using namespace std;

void function::analysis() {
    cin >> str;
    /*开始分析*/
    cout << "---开始递归下降分析---" << endl;
    //得到字符串长度
    codeLength = str.size();
    cout << "输入字符串的长度为：";
    cout << codeLength << endl;
    //设置第一个字符
    token = str.at(0);
    //将开始符号和'#'放入堆栈
    temp[0] = '#';
    cout << "---分析过程如下：---" << endl;
    E();

    /*输出结果*/
    if (token == str.at(str.size() - 2)) {
        token = str.at(str.size() - 1);
    }
    if (token == temp[0]) {
        cout << "---递归下降分析结束---" << endl;
        cout << "输出结果：" + str + "为合法字符串" << endl;
    } else {
        cout << "找不到结束符'#'" << endl;
        error();
    }
}

int function::match(char ch) {
    if (token == ch) {
        token = str.at(++i);
    } else {
        cout << "匹配失败，当前字符token与待匹配字符ch无法匹配 " << endl;
        error();
    }
    return 0;
}

int function::E() {
    cout << "使用产生式E->TG" << endl;
    T();
    G();
    return 0;
}

int function::T() {
    cout << "使用产生式T->FS" << endl;
    F();
    S();
    return 0;
}

int function::G() {
    if (token == '+') {
        cout << "使用产生式G->+TG" << endl;
        match('+');
        T();
        G();
    } else if (token == '-') {
        cout << "使用产生式G->-TG" << endl;
        match('-');
        T();
        G();
    } else {
        cout << "使用产生式G->ε" << endl;
    }
    return 0;
}

int function::F() {
    if (token == 'i') {
        cout << "使用产生式F->i" << endl;
        match('i');
    } else if (token == '(') {
        match('(');
        E();
        if (token == ')') {
            cout << "使用产生式F->(E)" << endl;
            match(')');
        } else {
            cout << "匹配')'失败" << endl;
            error();
        }
    } else {
        cout << "非法字符串" + str + "!" << endl;
        cout << "使用产生式F->(E)|i失败，*找不到待匹配的'('和'i'" << endl;
        error();
    }
    return 0;
}

int function::S() {
    if (token == '*') {
        cout << "使用产生式S->*FS" << endl;
        match('*');
        F();
        S();
    } else if (token == '/') {
        cout << "使用产生式S->/FS" << endl;
        match('/');
        F();
        S();
    } else {
        cout << "使用产生式S->ε" << endl;
    }
    return 0;
}

void function::error() {
    cout << "警告：递归下降分析程序异常结束！" << endl;
    exit(0);
}




