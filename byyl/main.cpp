﻿/*  
 @author : Song Jiaqi 
 @date : 2022-05-14 22:30  
*/
#include "function.h"

int main() {
    /*处理输出界面*/
    std::cout << "递归下降分析程序，编制人：宋嘉骐，20201209619，计科2002班" << std::endl;
    std::cout << "输入以#结束的符号串(包括+—*/（）i#)，在此位置输入符号串(例如：i+i*i#)：" << std::endl;
    function fc;
    fc.analysis();
    return 1;
}

