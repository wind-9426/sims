﻿/*  
 @author : Song Jiaqi 
 @date : 2022-05-14 22:33  
*/

#ifndef CLIONPROJECTS_C____FUNCTION_H
#define CLIONPROJECTS_C____FUNCTION_H

#include <iostream>

class function {
private:
    /*全局变量*/
    std::string str; //存储输入的字符串
    int i = 0; //字符串索引下标
    char temp[10]; //临时字符数组
    int codeLength; //输入字符串的长度
    char token; //当前字符
public:
    /*分析输入字符串*/
    void analysis();

    /*模块设计*/
    int match(char ch); //匹配终结符

    int E(); //E过程，对应产生式E->TG

    int T(); //T过程，对应产生式T->FS

    int G(); //G过程，对应产生式+TG|-TG|ε

    int F(); //F过程，对应产生式F->(E)|i

    int S(); //S过程，对应产生式s->*FS|/FS|ε

    /*错误处理*/
    void error();

};


#endif //CLIONPROJECTS_C____FUNCTION_H
