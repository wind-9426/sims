﻿#include "Student.h"
#include <sstream>
#include <iostream>


Student::Student(unsigned int Sno, const std::string &Sname, const std::string &Ssex, unsigned int Sage,
                 const std::string &Sdept, const std::string &Sclass)
//初始化参数列表
        : Sno(Sno), Sname(Sname), Ssex(Ssex), Sage(Sage), Sdept(Sdept), Sclass(Sclass) {

}

Student::Student() {

}

std::string Student::formatInfo() {
    std::stringstream ss;
    ss << std::endl << Sno << "\t" << Sname << "\t" << Ssex << "\t" << Sage << "\t" << Sdept << "\t" << Sclass
       << std::endl;
    return ss.str(); //返回字符串
}

void Student::formatWrite(const std::string &str) {
    //stringstream流的输入和输出操作。
    std::stringstream ss(str);
    ss >> this->Sno >> this->Sname >> this->Ssex >> this->Sage >> this->Sdept >> this->Sclass;
}

void Student::show() {
    std::cout << this->Sno << "\t" << this->Sname << "\t" << this->Ssex << "\t" << this->Sage << "\t" << this->Sdept
              << "\t" << this->Sclass << std::endl;
}

bool Student::operator==(const Student &right) const {
    return this->Sno == right.Sno;
}

bool Student::operator>(const Student &right) const {
    return this->Sno > right.Sno;
}

bool Student::operator<(const Student &right) const {
    return this->Sno < right.Sno;
}
