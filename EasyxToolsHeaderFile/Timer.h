﻿#ifndef CLIONPROJECTS_C____TIMER_H
#define CLIONPROJECTS_C____TIMER_H
#include <chrono> //处理日期和时间库
using namespace std::chrono;
class Timer {

public:
    //静态定时器
    static bool startTimer(int64_t ms, int id);
};


#endif //CLIONPROJECTS_C____TIMER_H
