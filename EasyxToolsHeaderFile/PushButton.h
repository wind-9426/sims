﻿#ifndef CLIONPROJECTS_C____PUSHBUTTON_H
#define CLIONPROJECTS_C____PUSHBUTTON_H
#include "BasicWidget.h"
#include "Configure.h"

class PushButton :/*继承BasicWidget*/
        public BasicWidget {
public:
    PushButton(const std::string &text = "Button", int x = 0, int y = 0, int w = 150, int h = 30);

    void show() ;

    bool isOn(); //鼠标是否在按钮上
    bool isClicked(); //按钮是否被点击

    void eventLoop(const ExMessage &msg); //事件循环,为m_msg进行赋值  &引用

    void setBackgroundColor(COLORREF c); ////设置默认背景颜色
    void setClickedColor(COLORREF c); //设置默认点击颜色

    void setBtnText(const char *text); //设置按钮文本

private:

    std::string m_text;
    ExMessage m_msg;
    /*按钮点击前后的颜色效果  COLORREF:color和reference的缩写，表示颜色值*/
    COLORREF current_c = RGB(180, 227, 245);
    COLORREF normal_c = RGB(180, 227, 245);
    COLORREF after_c = RGB(40, 192, 179);
};


#endif //CLIONPROJECTS_C____PUSHBUTTON_H
