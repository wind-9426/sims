﻿#ifndef CLIONPROJECTS_C____TABLE_H
#define CLIONPROJECTS_C____TABLE_H

#include "BasicWidget.h"
#include "Configure.h"
#include <vector>
#include "PushButton.h"

class Table :
        public BasicWidget {
public:
    //表格的行和列
    Table(int row = 0, int column = 0);

    //设置行
    void setRow(int row);

    //设置列
    void setColumn(int column);

    //输出
    void show();

    //表头
    void setHeader(const std::string &header);

    //绘制表头
    void drawTableHeader();

    //绘制表格
    void drawTableGrid();

    //绘制表格下的按钮
    void drawTableButton();

    //事件循环，实现表格下按钮的点击效果
    void eventTableLoop(const ExMessage &t_msg);

    //绘制数据
    void drawTableData();

    //将信息插入到表格中
    void insertData(const std::string &data);

    /*清除*/
    void clearTableData();

    //分割字符串
    static std::vector<std::string> split(std::string str, char separator = '\t');

    /*分页处理*/
    void updatePage(); //更新页，在插入数据后进行更新操作



private:
    //成员变量--行和列
    int m_rows;
    int m_cols;
    //表头名
    std::string m_headName;
    //学生信息数据
    std::vector<std::string> m_data;
    //表格格子的宽度和高度
    int m_gridW;
    int m_gridH;
    //文字的宽度和高度
    int m_tW;
    int m_tH;

    /*分页处理需要的成员变量*/
    int m_currentPage; //当前页
    int m_maxPage; //最大页数
    int m_extraData; //最后一页但非整数页的剩余数据

    /*分页处理按钮指针变量*/
    PushButton m_previousBtn; //上一页
    PushButton m_nextBtn; //下一页
    PushButton m_firstBtn; //第一页
    PushButton m_lastBtn; //最后一页


};

#endif//CLIONPROJECTS_C____TABLE_H
