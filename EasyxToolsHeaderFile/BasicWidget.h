﻿#ifndef CLIONPROJECTS_C____BASICWIDGET_H
#define CLIONPROJECTS_C____BASICWIDGET_H

/*基类*/
class BasicWidget {
protected:
    int m_x;
    int m_y;
    int m_w;
    int m_h;

public:
    /*加~表示此函数是析构函数,与构造函数相反。
     * Table类继承了BasicWidget类，基类析构函数要加上 virtual*/
    virtual ~BasicWidget(){}

    //含参构造
    BasicWidget(int x, int y, int w, int h);

    int x();

    int y();

    int width();

    int height();

    //设置窗口的宽度和高度
    void setFixedSize(int w, int h);

    //获取移动坐标
    void movePosition(int x, int y);

    //每个窗口都要有一个show()函数
    virtual void show() = 0;

};


#endif //CLIONPROJECTS_C____BASICWIDGET_H
