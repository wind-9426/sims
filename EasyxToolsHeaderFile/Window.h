﻿#ifndef CLIONPROJECTS_C____WINDOW_H
#define CLIONPROJECTS_C____WINDOW_H
/*窗口类*/
#include "Configure.h"
class Window {
public:
    /*创建一个窗口*/
    Window(int w,int h,int flag);
    //设置窗口标题
    void setWindowTitle(const std::string &title);
    /*用指定的程序替换当前进程的所有内容*/
    int exec();

    //封装
    static int width();
    static int height();
    static void clear();
    static void beginDraw();
    static void flushDraw();
    static void endDraw();

    //按键和鼠标操作
    inline static bool hashMsg(){
        //bool:获取一个消息，并立即返回，用于判断是否获取消息
        return ::peekmessage(&m_msg,EM_MOUSE|EM_KEY);
    }
    inline static const ExMessage& getMsg(){
        return m_msg;
    }


private:
    HWND m_handle; //HWND窗口句柄,相当于身份证
    static ExMessage m_msg; //鼠标和按键消息
};

#endif //CLIONPROJECTS_C____WINDOW_H
