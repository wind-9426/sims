﻿#include "Management.h"
#include <iostream>

int main(){
    Window w(1280,720,EW_SHOWCONSOLE);//显示控制台
    w.setWindowTitle("学生信息管理系统");
    Management m;
    m.run();
    return w.exec();//防止闪退
}

//
//int main(){
//    auto res = Table::split("190101\t\taa\t男\t20\t网络工程\t2001");
//    for(auto& stuInfo:res){
//        std::cout<<stuInfo<<" ";
//    }
//    return 0;
//}