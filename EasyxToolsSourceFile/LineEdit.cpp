﻿#include "../EasyxToolsHeaderFile/LineEdit.h"
//#include"Window.h"
//#include<conio.h>

/*继承基类*/
LineEdit::LineEdit(int x, int y, int w, int h)
:BasicWidget(x, y, w, h), textw(2), m_isPopUp(false)
, m_title("请输入")
{

}

void LineEdit::show()
{
    //设置长方形编辑框的填充色
    setfillcolor(RGB(232, 232, 236));
    //设置长方形编辑框的大小
    fillrectangle(m_x, m_y, m_x+m_w, m_y+m_h);

    if (!m_hideCursor)
    {
        //设置红色提示线
        setlinecolor(RED);
        line(m_x + textw + 2, m_y + 2, m_x + textw + 2, m_y + m_h - 2);

        //颜色复原
        setlinecolor(BLACK);
    }

    //把文字输出到edit上
    settextstyle(20,0,"楷体");
    outtextxy(m_x, m_y+(m_h - textheight(m_text.c_str())) / 2,m_text.c_str());

    if (m_isPopUp)
    {
        m_pretext = m_text;
        popInputBox();
        m_isPopUp = false; //弹出一次后关闭
    }
}

//判断是否需要弹出输入框---在框的范围内且鼠标左键按下
void LineEdit::eventLoop(const ExMessage& msg)
{

    if (msg.x > m_x && msg.x < m_x + m_w && msg.y > m_y && msg.y < m_y + m_h
    && msg.message == /*左键按下消息*/WM_LBUTTONDOWN)
    {
        m_isPopUp = true;
    }
}

void LineEdit::popInputBox()
{
    char str[128] = { 0 };
    //InputBox函数以对话框形式获取用户输入
    InputBox(str, 128, nullptr,m_title.c_str(),m_text.c_str());
    m_text = str;
    textw = ::textwidth(m_text.c_str());
}

void LineEdit::setInputBoxTitle(const std::string& title)
{
    m_title = title;
}

void LineEdit::setText(const std::string& text)
{
    m_text = text;
    textw = ::textwidth(m_text.c_str());
}

std::string  LineEdit::text()
{
    return m_text;
}

void LineEdit::clear()
{
    m_text.clear();
    textw = 0;
}

void LineEdit::setCursorHide(bool isHide)
{
    m_hideCursor = isHide;
}

bool LineEdit::textChanged()
{
    if (m_pretext == m_text)
    {
        return false;
    }
    m_pretext = m_text;	//更新,通知一次文本改变即可
    return true;
}