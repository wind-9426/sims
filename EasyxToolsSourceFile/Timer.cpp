﻿#include "../EasyxToolsHeaderFile/Timer.h"

bool Timer::startTimer(int64_t ms, int id)
{
    static int64_t start[21] = { 0 };
    int64_t end = clock();
    if (end - start[id] >= ms)
    {
        start[id] = end;
        return true;
    }
    return false;
}
