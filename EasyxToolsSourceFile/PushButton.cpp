﻿#include "../EasyxToolsHeaderFile/PushButton.h"

PushButton::PushButton(const std::string &text, int x, int y, int w, int h)
        : BasicWidget(x, y, w, h), m_text(text) //:定义BasicWidget类的继承
{

}

void PushButton::show() {
    //设置按钮颜色
    setfillcolor(current_c);
    //设置圆角矩形按钮
    ::fillroundrect(m_x, m_y, m_x + m_w, m_y + m_h, 10, 10);

    //设置文字颜色
    settextcolor(RGB(6, 6, 6));
    //设置文字居中显示在按钮中间
    int xPos = m_x + (m_w - textwidth(m_text.c_str())) / 2;
    int yPos = m_y + (m_h - textheight(m_text.c_str())) / 2;
    //指定位置输出字符串
    ::outtextxy(xPos, yPos, m_text.c_str());
}

bool PushButton::isOn() {
    if (m_msg.x >= m_x && m_msg.x <= m_x + m_w && m_msg.y >= m_y && m_msg.y <= m_y + m_h) {
        return true;
    }
    return false;
}

//只支持识别鼠标左键
bool PushButton::isClicked() {
    if (isOn()) {
        if (m_msg.message == WM_LBUTTONDOWN) {//WM_LBUTTONDOWN 左键按下消息
            return true;
        }
    }
    return false;
}

void PushButton::eventLoop(const ExMessage &msg) {
    m_msg = msg;
    //设置鼠标在按钮和不在按钮上的颜色效果
    if (!isOn()) {
        current_c = normal_c;
    } else {
        current_c = after_c;
    }
}

void PushButton::setBackgroundColor(COLORREF c) {
    normal_c = c;
}

void PushButton::setClickedColor(COLORREF c) {
    after_c = c;
}

void PushButton::setBtnText(const char *text) {
    m_text = text;
}
