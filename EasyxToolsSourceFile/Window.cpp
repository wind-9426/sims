﻿#include "../EasyxToolsHeaderFile/Window.h"
#include <iostream>
ExMessage Window::m_msg;

Window::Window(int w, int h, int flag) {
    //::表示全局函数，而不是成员函数
    m_handle = ::initgraph(w,h,flag);
    //设置当前设备图案填充和文字输出时的背景模式
    ::setbkmode(TRANSPARENT);//设为透明色
}

int Window::exec() {
    return getchar();
}

void Window::setWindowTitle(const std::string &title) {
    ::SetWindowText(m_handle,title.c_str());
}

int Window::width() {
    return ::getwidth(); //获取绘图区宽度
}

int Window::height() {
    return ::getheight(); //获取绘图区高度
}

void Window::clear() {
    ::cleardevice(); //使用当前背景色清空绘图设备。
}

void Window::beginDraw() {
    ::BeginBatchDraw(); //开始批量绘图
}

void Window::flushDraw() {
    ::FlushBatchDraw(); //执行未完成的绘制任务
}

void Window::endDraw() {
    ::EndBatchDraw(); //结束批量绘制，并执行未完成的绘制任务
}
