﻿#ifndef CLIONPROJECTS_C____MANAGEMENT_H
#define CLIONPROJECTS_C____MANAGEMENT_H
#include "Student.h"
#include "EasyxToolsHeaderFile/Configure.h"
#include "EasyxToolsHeaderFile/Window.h"
#include <vector> //一个动态数组，它的元素是连续存储的
#include "EasyxToolsHeaderFile/PushButton.h"
#include "EasyxToolsHeaderFile/Table.h"
#include "EasyxToolsHeaderFile/LineEdit.h"
#include "EasyxToolsHeaderFile/Timer.h"

class Management {
private:
    enum Operator {/*枚举*/
        Help,
        Show,
        Add,
        Delete,
        Modify,
        Search,
        Exit,
        Menu = 666
    };

    int op = Menu; //用户选择option
    IMAGE m_bg;
    ExMessage m_msg;
    std::vector<PushButton *> menu_btns;

    /*待读取学生信息文件的表头*/
    std::string m_headName;
    /*学生的动态数组*/
    std::vector<Student> vec_stu;
    /*查看表格*/
    Table* m_showTable;

    /*增删改查功能实现需要的变量*/
    //1.增加
    LineEdit	m_addEdit; //添加学生编辑框
    PushButton	m_addBtn; //添加学生确认按钮
    //2.删除
    LineEdit*	delEdit;
    PushButton* delBtn;
    Table*		delTable;
    //3.修改
    LineEdit	*m_modifyEdit; //添加学生编辑框
    std::vector<LineEdit*> m_stuEdits;
    std::vector<Student>::iterator m_modifyIt; //指向要修改的学生
    bool haveStu = false; //是否有要修改的学生
    //4.查找
    LineEdit* searchEdit;
    PushButton* searchIdBtn;
    PushButton* searchNameBtn;
    Table* searchTable;


public:
    //无参构造
    Management();

    //事件循环
    void eventLoop();

    //启动管理类
    void run();

    //菜单
    int menu();

    //0.帮助查看使用
    void help();

    //1.查看已存学生信息
    void showStuInfo();

    //2.增加学生信息
    void addStuInfo();

    //3.删除学生信息
    void deleteStuInfo();

    //4.修改学生信息
    void modifyStuInfo();

    //5.查找学生信息
    void searchStuInfo();

    //6.退出系统
    void exitSystem();

    //绘制背景
    void drawBackground();



    /*实现读写学生信息*/
    void readFile(const std::string& fileName);
    void writeFile(const std::string& fileName);

    /*增删*/
    //插入和删除数据时，更新一下表格
    void updateTableData();

};

#endif //CLIONPROJECTS_C____MANAGEMENT_H
