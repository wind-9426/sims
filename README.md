# 学生信息管理系统

#### 介绍
使用c++和c++图形库Easyx制作的一个学生信息管理系统。

#### 软件架构
Clion 2021.2


#### 如何导入Easyx库？
1.去官网下载Eaysx库安装文件，没记错的话，下好应该是个exe文件，然后将后缀改为.7z等压缩包格式，提取其中的
lib和include文件夹
2.在CMakeLists.txt中写入以下代码：
注意set这个位置填写的是你的Easyx安装路径，你把那两个文件夹放在哪里就去写哪里。

set(EasyX_INC F:\\EasyX\\include)
set(EasyX_LINK F:\\EasyX\\lib\\VC2015\\x64)
include_directories(${EasyX_INC})
link_directories(${EasyX_LINK})

target_link_libraries(CLionProjects_c___ ${EasyX_LINK})

#### 使用说明

1.  byyl和poker不是学生管理系统的内容，是我做的两个实验，没有分开，下载后可以自行删掉。
2.  本人喜欢使用clion进行c++的学习，无奈Easyx库是vc专属库，所以用起来会比较折腾，需要在CMakeLists.txt中自己导入库和头文件
3.  代码仅供参考，并不是特别完美的那种，个人也还在学习中。
4.  给没有太多编程经验或者小白提一嘴，我的GUI界面背景的图片以及事先写好的学生信息.txt的话是包括在这个项目内的，但是你下载好是在c盘的desktop，而我的代码里设置的有我自己的路径，所以不要直接运行哦，否则一片黑色，有必要的话去代码相应位置修改路径，然后把图片和学生信息放在你指定的路径位置。
