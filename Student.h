﻿#ifndef CLIONPROJECTS_C____STUDENT_H
#define CLIONPROJECTS_C____STUDENT_H

#include "EasyxToolsHeaderFile/Configure.h"
#include <vector>
class Student {
public:
    unsigned int Sno;
    std::string Sname;
    std::string Ssex;
    unsigned int Sage;
    std::string Sdept;
    std::string Sclass;

public:
    /*操作符重载,自定义比较方法*/
    bool operator==(const Student& right) const;
    bool operator>(const Student& right) const;
    bool operator<(const Student& right) const;

public:
    //无参构造
    Student();
    //含参构造
    Student(unsigned int Sno, const std::string &Sname, const std::string &Ssex, unsigned int Sage,
            const std::string &Sdept, const std::string &Sclass);
    //写信息时格式化数据
    std::string formatInfo();
    //学生信息转字符串
    void formatWrite(const std::string& str);

    //显示学生信息
    void show();
};


#endif //CLIONPROJECTS_C____STUDENT_H
